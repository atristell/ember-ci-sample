module.exports = {
  test_page: 'tests/index.html?hidepassed',
  disable_watching: true,
  framework: "qunit",
  //parallel: 2,
  bs_user: "USER",
  bs_key: "KEY",
  //before_tests: "./scripts/browserstack-local.js &",
  //on_start: {
    //command: "ls -la",
    //command: "kill -9 $(ps -A | grep BrowserStackLocal | grep -v grep | cut -d ' ' -f2); ./scripts/browserstack-local.js &",
    //command: "node ./scripts/browserstack-local.js &",
    //wait_for_text: "Tunnel Started",
    //wait_for_text_timeout: 300000
  //},
  /* on_exit: {
    command: "./scripts/stop-BrowserStackLocal.js `cat browserStackLocal.pid`; rm browserstack-local.pid",
  }, */
  launch_in_ci: [
    'Chrome', 'Firefox' //,bs_firefox
  ],
  launch_in_dev: [
    'Chrome'
  ],
  launchers: {
    bs_firefox: {
      command: "node run_on_browserstack.js Windows 10 firefox latest nil <url>",
      protocol: "browser"
    }
  },
  browser_args: {
    Chrome: {
      ci: [
        // --no-sandbox is needed when running Chrome inside a container
        process.env.CI ? '--no-sandbox' : null,
        '--headless',
        '--disable-gpu',
        '--disable-dev-shm-usage',
        '--disable-software-rasterizer',
        '--mute-audio',
        '--remote-debugging-port=0',
        '--window-size=1440,900'
      ].filter(Boolean)
    },
    Firefox: {
      ci: [
        // --no-sandbox is needed when running Chrome inside a container
        process.env.CI ? '--no-sandbox' : null,
        '--headless',
        '--disable-gpu',
        '--disable-dev-shm-usage',
        '--disable-software-rasterizer',
        '--mute-audio',
        '--remote-debugging-port=0',
        '--window-size=1440,900'
      ].filter(Boolean)
    }
  }
};
